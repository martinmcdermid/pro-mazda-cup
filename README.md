# Welcome to the Friday Night Pro Mazda Cup league.

The Friday Night Pro Mazda Cup is a fixed iRacing league that runs every Friday at 8:30pm GMT. Ran by the folks at Sendit eSports, we strive to provide close, fun and competitive racing using the Pro Mazda and using as much free content as possible. 

Discord: https://discord.gg/SzAfYYG

Website: https://members.iracing.com/membersite/member/LeagueView.do?league=6483

Facebook Group: https://www.facebook.com/Friday-Night-Pro-Mazda-Cup-League-103212471814093
